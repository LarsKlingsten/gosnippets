package gosnippets

import (
	"fmt"
)

const stringPad = 30

// UserSetLogLevel determines whether log is displayed or
var UserSetLogLevel LogLevel

// LogString set common log string, that all logs inclde
var LogString = ""

//Log print out log message with time stamp
func Log(msg string) {
	fmt.Printf("%s %s %s \n", NowToString(), LogString, msg)
}

//Logf print out formattet (like Sprintf) log message with time stamp
func Logf(format string, args ...interface{}) {
	fmt.Printf(NowToStringMs()+" "+format+"\n", args...)
}

//Log2 print out log message with time stamp - depends on LogLevel Bitmask above
func Log2(funcLogLevel LogLevel, msg string) {
	if IsBitmaskSet(UserSetLogLevel, funcLogLevel) {
		logStr := PadZero(int(funcLogLevel), 2)
		fmt.Printf("%s log=%s%s %s \n", NowToString(), logStr, LogString, msg)
	}
}

//LogMs print out log message with time stamp incl miliseconds - depends on LogLevel Bitmask above
func LogMs(funcLogLevel LogLevel, msg string) {
	if IsBitmaskSet(UserSetLogLevel, funcLogLevel) {
		logStr := PadZero(int(funcLogLevel), 2)
		fmt.Printf("%s log=%s%s %s \n", NowToStringMs(), logStr, LogString, msg)
	}
}

//LogFatal print out log message with time stamp
func LogFatal(msg string) {
	fmt.Printf("%s %s%s \n", NowToString(), LogString, msg)
}

//LogErr print out log message with time stamp - always prints
func LogErr(msg string, err error) {
	fmt.Printf("%s %s%s err=%s\n", NowToString(), LogString, msg, err.Error())
}

//LogErr2 print out log message with time stamp - print only if LogLevel incl LogError (uses Log2(LogError,"msg")
func LogErr2(msg string, err error) {
	Log2(LogError, f("%s err=%s\n", msg, err.Error()))
}

// LogIfPerformanceFlag returns bool whether or not performance will be logged
func LogIfPerformanceFlag() bool {
	return IsBitmaskSet(UserSetLogLevel, LogPerformance)
}

//IsBitmaskSet returns true if a bit is set (such a using --loglevel=64 or --loglevel=127)
func IsBitmaskSet(bitmask LogLevel, test LogLevel) bool {
	return bitmask&test != 0
}

// LogLevel is the various log type that a message can carry
type LogLevel uint

// type of messages
const (
	LogError          LogLevel = 1
	LogInitMsg        LogLevel = 2
	LogDebug          LogLevel = 4
	LogCache          LogLevel = 8
	LogRequest        LogLevel = 16
	LogAuthentication LogLevel = 32
	LogPerformance    LogLevel = 64
	LogMemUsage       LogLevel = 128
	LogToFile         LogLevel = 256
	LogSQL            LogLevel = 512
	LogSSL            LogLevel = 1024
)

//UintToLogLevel converts an uint to Loglevel (or just do 'snippets.LogLevel(value)' )
func UintToLogLevel(value uint) LogLevel {
	return LogLevel(value)
}

// LogFuncname prints log message - and also function names.
func LogFuncname(msg string) {
	Log(f("%s %s", FunctionNameByStackLevel(3), msg))
}

// LogTestWithFuncName for test reuslts
func LogTestWithFuncName(number int) {
	name := StringPadRight(FunctionNameByStackLevel(3), stringPad, " ")
	Log(f("%s ran %2d tests", name, number))
}
