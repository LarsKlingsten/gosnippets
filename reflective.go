package gosnippets

import (
	"fmt"
	"reflect"
	"runtime"
	"strings"
	"time"
)

// FunctionNameByStackLevel return the function name
func FunctionNameByStackLevel(level int) string {
	str := FunctionNameAndPathByStackLevel(level + 1) // we are adding +1 as this is an intermediate function
	index := strings.LastIndex(str, ".")
	if index < 0 {
		return str
	}
	return str[index+1:] // safe to add +1 (as a function can't end with a ".") and we do not leading "."
}

// FunctionNameAndPathByStackLevel return the function name with path
func FunctionNameAndPathByStackLevel(level int) string {

	fpcs := make([]uintptr, 1)
	n := runtime.Callers(level, fpcs)
	if n == 0 {
		return f("function calls to stack %d was not found", level)
	}
	function := runtime.FuncForPC(fpcs[0] - 1)
	if function == nil {
		return f("function calls to stack %d was not found (Reflection on FuncForPC, if you must know)", level)
	}
	return function.Name()
}

// FunctionPrintStackNames print the names of callee function
// note: does NOT return level 0 or 1 as 0=runtime.Callers and FunctionNameByStackLevel
func FunctionPrintStackNames() {
	for i := 1; i < 6; i++ {
		p(i, FunctionNameByStackLevel(i))
	}
}

// FunctionFindName search for string within the function name -> ".Test" return all tests functions
func FunctionFindName(funcName string) string {
	for i := 2; i < 4; i++ {
		name := FunctionNameAndPathByStackLevel(i)
		p(name)
		if strings.Contains(name, funcName) {
			return name
		}
	}
	return funcName + " was not found"
}

// MemoryUsage displays memory usage to the commandline (usage via 'go' function call )
// use seconds = 0 to call only once - otherwise use 'go MemoryUsage(5)'
// https://gist.github.com/thomas11/2909362
func MemoryUsage(seconds uint) {

	if seconds == 0 {
		var m runtime.MemStats
		runtime.ReadMemStats(&m)
		Log(f("Alloc = %v TotalAlloc = %v  Sys = %v NumGC = %v ", m.Alloc/1024, m.TotalAlloc/1024, m.Sys/1024, m.NumGC))
		return
	}

	// set to repeat memory
	for {
		var m runtime.MemStats
		runtime.ReadMemStats(&m)
		Log(f("Alloc = %v TotalAlloc = %v  Sys = %v NumGC = %v ", m.Alloc/1024, m.TotalAlloc/1024, m.Sys/1024, m.NumGC))
		time.Sleep(time.Duration(seconds) * time.Second)
	}
}

// ReflectSetField converts a map of string:interface to a struct object
//
// Usage: FillStruct converts the single db row (which is a map[string]interface{})
//
//	       to a single PrimaryKey (your own struct) instance
//
//	        func (primaryKey *PrimaryKey) FillStruct(m map[string]interface{}) (*PrimaryKey, error) {
//		       for k, v := range m {
//		        	err := sn.ReflectSetField(tblreceivegroups, k, v)
//			        if err != nil {
//				        return nil, fmt.Errorf("@FillStruct. 'TblReceiveGroups' does not match your database Perhaps run it again err=%v ", err)
//			        }
//		        }
//		        return primaryKey, nil
//	}
func ReflectSetField(obj interface{}, name string, value interface{}) error {
	// we do not pass an error back, as nil values are in go handled with default values ('nil value string are always "")
	if value == nil {
		return nil
	}
	structValue := reflect.ValueOf(obj).Elem()
	structFieldValue := structValue.FieldByName(name)

	if !structFieldValue.IsValid() {
		return fmt.Errorf("no such field: %s in obj", name)
	}
	if !structFieldValue.CanSet() {
		return fmt.Errorf("cannot set %s field value (perhaps you made field private)", name)
	}

	structFieldType := structFieldValue.Type()
	val := reflect.ValueOf(value)

	if structFieldType != val.Type() {
		return fmt.Errorf("provided value %v type didn't match obj field type", value)
	}
	structFieldValue.Set(val)
	return nil
}
