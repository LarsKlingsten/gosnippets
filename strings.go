package gosnippets

import (
	"bytes"
	"fmt"
	"io"
	"mime/quotedprintable"
	"regexp"
	"strconv"
	"strings"
	"unicode"
	"unicode/utf8"
)

// ValididateStringLen validates whether a string is with a suitable length
// used to ensure length of a string to do exceed database max len
func ValididateStringLen(valueStr string, minLen int, maxLen int) bool {
	length := len(valueStr)
	if length < minLen || length > maxLen {
		return false
	}
	return true
}

// PadZero convert an integer to a string and add "0" infront, calls PadZeroInt64
func PadZero(number int, length int) string {
	return PadInt64(int64(number), length, "0")
}

// PadInt64 convert an integer to a string and add "0" or other padding infront
func PadInt64(number int64, length int, padding string) string {

	if len(padding) == 0 || length < 0 {
		return f("%d", number)
	}

	// convert number to string, disregarding sign
	numberStr := fmt.Sprintf("%d", Abs64(number))

	numberStrLen := length - len(numberStr)
	for i := 0; i < numberStrLen; i++ {
		numberStr = padding + numberStr
	}

	if number < 0 {
		numberStr = "-" + numberStr
	}
	return numberStr
}

// RemoveStringTabs removes tabs from a string
func RemoveStringTabs(s string) string {
	return strings.Replace(s, DefaultTab, "", -1)
}

// StringRemoveNonAlphaNum removes Non Alpha and nummeric numbers from string
func StringRemoveNonAlphaNum(s string) string {
	req, _ := regexp.Compile("[^a-zA-Z0-9]+")
	return req.ReplaceAllString(s, "")
}

// StringRenoveNonSQLchars allows [a-zA-Z0-9 =<>?*()]+"
func StringRenoveNonSQLchars(s string) string {
	req, _ := regexp.Compile("[^a-zA-Z0-9 =<>?*(),._]+")
	return req.ReplaceAllString(s, "")
}

// StringRemoveInvalidEmailChars simplified removal of invalid chars in emails
// - but does not check special cases, such as '..', or "." at the begining etc
// https://en.wikipedia.org/wiki/Email_address#Local_part
func StringRemoveInvalidEmailChars(s string) string {
	req, _ := regexp.Compile("[^a-zA-Z0-9@!#$%&'*+-/=?^_`{|}~]+")
	return req.ReplaceAllString(s, "")
}

// StringPad is deprecated! Use instead StringRepeat, or alternatively see StringPadLeft or StringPadRight
func StringPad(s string, times int) string {
	return StringRepeat(s, times)
}

// StringRepeat repeats a string
func StringRepeat(s string, times int) (result string) {
	for i := 0; i < times; i++ {
		result += s
	}
	return
}

// StringPadLeft string with number times
func StringPadLeft(str string, length int, pad string) string {
	return StringRepeat(pad, length-len(str)) + str
}

// StringPadRight string with number times
func StringPadRight(str string, length int, pad string) string {
	return str + StringRepeat(pad, length-len(str))
}

// StringLeft returns the left chars of a string
func StringLeft(str string, length int) string {
	if length > len(str) {
		length = len(str)
	}
	return str[:length]
}

// StringRight returns the right chars of a string
func StringRight(str string, length int) string {
	if length > len(str) {
		length = len(str)
	}
	return str[len(str)-length:]
}

// StringToFloat parsing strings to float
func StringToFloat(item string, ukFormat bool) (float64, error) {

	if ukFormat { // try standard UK/US with ,=thousand separator "."=decimal
		itemUK := strings.Replace(item, ",", "", 99) // remove thousand seperator
		myFloat, err := strconv.ParseFloat(itemUK, 64)
		if err == nil {
			return myFloat, nil
		}
	}

	// swap thousand seperator and decimals seperators
	itemNoDot := strings.Replace(item, ".", "", 99)
	itemNoDot = strings.Replace(itemNoDot, ",", ".", 99)

	return strconv.ParseFloat(itemNoDot, 64) // try to convert string to float
}

// ParseStringToFloat  parsing strings to float
// @Deprecated: use StringToFloat
func ParseStringToFloat(item string, ukFormat bool) (float64, error) {
	return StringToFloat(item, ukFormat)
}

// StringRemove removes multiple string from an slice
func StringRemove(s []string, r string) []string {
	for i := 0; i < len(s); i++ {
		if s[i] == r {
			s = append(s[:i], s[i+1:]...)
			i--
		}
	}
	return s
}

func StringDecodeQuotedPrintableString(encodedString string) string {
	return StringDecodeQuotedPrintableBytes([]byte(encodedString))
}

func StringDecodeQuotedPrintableBytes(encodedBytes []byte) string {

	var decodedBytes []byte

	qp := quotedprintable.NewReader(bytes.NewBuffer(encodedBytes)) // Create a quoted-printable reader.

	for { // Decode the quoted-printable data into a byte slice.
		buffer := make([]byte, 4096)
		n, err := qp.Read(buffer)
		if err == io.EOF {
			decodedBytes = append(decodedBytes, buffer[:n]...)
			break
		}
		if err != nil {
			return ""
		}

		decodedBytes = append(decodedBytes, buffer[:n]...)
	}

	return string(decodedBytes)
}

// not tests have been made for below

// StringBefore return the value before the delimitor
// https://www.dotnetperls.com/between-before-after-go
// -> StringBefore("test<adr@mail.com>","<") -> "test"
func StringBefore(s string, match string) string {
	// Get substring before a string.
	pos := strings.Index(s, match)
	if pos == -1 {
		return ""
	}
	return s[0:pos]
}

// StringBetween returns the value between to delimitors
// -> StringBetween("test<adr@mail.com>","<",">") -> "adr@mail.com"
// https://www.dotnetperls.com/between-before-after-go
func StringBetween(s string, match1 string, match2 string) string {
	// Get substring between two strings.
	posFirst := strings.Index(s, match1)
	if posFirst == -1 {
		return ""
	}
	posLast := strings.Index(s, match2)
	if posLast == -1 {
		return ""
	}
	posFirstAdjusted := posFirst + len(match1)
	if posFirstAdjusted >= posLast {
		return ""
	}
	return s[posFirstAdjusted:posLast]
}

// StringAfter returns the value after the delimitor
// https://www.dotnetperls.com/between-before-after-go
func StringAfter(s string, match string) string {
	// Get substring after a string.
	pos := strings.LastIndex(s, match)
	if pos == -1 {
		return ""
	}
	adjustedPos := pos + len(match)
	if adjustedPos >= len(s) {
		return ""
	}
	return s[adjustedPos:]
}

// StringTrimAndLCase removes leading and trailing spaces, and goes for lowercase
func StringTrimAndLCase(s string) string {
	return StringTrim(strings.ToLower(s))
}

// StringTrim removes leading and trailing spaces
func StringTrim(s string) string {
	return strings.Trim(s, " ")
}

// StringSetFirstCharToUCase sets the first char to uppercase
func StringSetFirstCharToUCase(name string) string {
	const low = 'A'
	const high = 'Z'
	firstChar := name[0]
	if firstChar >= low && firstChar <= high {
		return name
	}
	bytes := []byte(name)
	bytes[0] = byte(unicode.ToUpper(rune(firstChar)))
	return string(bytes)
}

// https://henvic.dev/posts/go-utf8/
func StringShortenAsNecessary(s string, maxLength int) string {
	if utf8.RuneCountInString(s) > maxLength {
		rs := []rune(s)
		return string(rs[:maxLength])
	}
	return s
}

func StringHasNullTerminator(str string) bool {
	return strings.Contains(str, "\x00")
}

func StringRemoveNullTerminator(str string) string {
	return strings.ReplaceAll(str, "\x00", "")
}

// extracts domain from an email address,
// example: "lar@gmail.com.sg" -> "gmail.com.sg"
func StringExtractEmailDomain(emailadr string) string {

	withinTags := StringExtractWithinTags(emailadr)
	if withinTags == "" {
		withinTags = emailadr
	}

	reDomain := regexp.MustCompile(`@(.*)$`)
	domain := reDomain.FindString(withinTags)
	domain = strings.TrimLeft(domain, "@") // remove the '@'
	return domain
}

// return string between '<' and '>'
// or "" if '<' and '>' were not found
func StringExtractWithinTags(str string) string {
	re := regexp.MustCompile(`<([^>]+)>`)
	matches := re.FindAllStringSubmatch(str, -1)
	if len(matches) == 0 {
		return ""
	}
	return matches[len(matches)-1][1]
}
