package gosnippets

import (
	"encoding/json"
	"errors"
	"reflect"
	"time"
)

// IsString check whether or not a value is a a string
func IsString(value interface{}) bool {
	switch reflect.ValueOf(&value).Elem().Interface().(type) {
	case string:
		return true
	default:
		return false
	}
}

// ConvertRFC3339StringToTime converts a value containing a RCF3339 formatted string to a GO time.Time
func ConvertRFC3339StringToTime(value interface{}) (time.Time, error) {
	const format string = time.RFC3339
	badTime := time.Now()
	stdError := errors.New("not a RFC3339 date")

	if !IsString(value) {
		return badTime, stdError
	}
	valueStr := value.(string)

	if !IsDateRFC3339(valueStr) {
		return badTime, stdError
	}

	// try to parse
	date, err := time.Parse(format, valueStr)
	if err != nil {
		return badTime, stdError
	}

	return date, nil

}

func StructToMap(val interface{}) map[string]interface{} {
	var mapInfo map[string]interface{}
	info_as_bytes, _ := json.Marshal(val)
	json.Unmarshal(info_as_bytes, &mapInfo)
	return mapInfo
}
