package gosnippets

import (
	"testing"
)

func TestRandomInt(t *testing.T) {
	min := 1
	max := 9
	for i := 0; i < 100; i++ {
		result := RandomInt(1, 9)
		if result < min || result > max {
			t.Errorf("@Snippets.RandomInt, result %d was either large than %d or small than %d", result, min, max)
		}
	}
	LogTestWithFuncName(1)
}
