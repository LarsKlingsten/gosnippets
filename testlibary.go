package gosnippets

import "encoding/json"

//IsValidJSON evaluates whether a string is valid JSON
func IsValidJSON(str string) bool {
	var js json.RawMessage
	return json.Unmarshal([]byte(str), &js) == nil
}

//IsValidJSONbytes evaluates whether a []Byte is valid JSON
func IsValidJSONbytes(bytes []byte) bool {
	var js json.RawMessage
	return json.Unmarshal((bytes), &js) == nil
}
