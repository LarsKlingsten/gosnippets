# GO-Snippets

## version 
* 0.0.1 # first version
* 0.0.2 # StructToMap
* 0.0.3 # adds StringShortenAsNecessary, StringHasNullTerminator, StringRemoveNullTerminator
* 0.0.5 # adds DecodeQuotedPrintableBytes, DecodeQuotedPrintableString
* 0.0.6 # adds StringDecodeQuotedPrintableBytes, StringDecodeQuotedPrintableString
* 0.0.7 # adds StringExtractEmailDomain, StringExtractWithinTags

### update version
```sh
git tag -a v0.0.7 -m "version 0.0.7"
git push --tags

https://pkg.go.dev/bitbucket.org/larsklingsten/gosnippets@v0.0.7
```

## test 
```
go test -bench .
go test
```

### LogLevels
*	LogError          LogLevel = 1
*	LogInitMsg        LogLevel = 2
*	LogDebug          LogLevel = 4
*	LogCache          LogLevel = 8
*	LogRequest        LogLevel = 16
*	LogAuthentication LogLevel = 32
*	LogPerformance    LogLevel = 64
*	LogMemUsage       LogLevel = 128
*	LogToFile         LogLevel = 256
*	LogSQL            LogLevel = 512