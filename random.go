package gosnippets

import (
	"math/rand"
	"time"
)

// seeds, appears best to go here
var myRnd = time.Since(time.Now()).Nanoseconds()
var r = rand.New(rand.NewSource(myRnd))

// RandomInt -> return a 'random' number between, and inclusive both from and to
func RandomInt(from int, to int) int {
	return r.Intn(to) + from
}

// UniqueString return a nano seconds from epoch (which may be unique enough)
func UniqueString() string {
	return f("%v", time.Now().UnixNano()/100)
}
