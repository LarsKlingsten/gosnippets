package gosnippets

import "time"
import "strings"

const timeFormat string = "2006-01-02 15:04:05"
const timeFormatSQLDateTime string = "2006-01-02 15:04:05.000"

// DateMax return 2098-03-12 (utc)
func DateMax() time.Time {
	return time.Date(2098, 3, 12, 0, 0, 0, 0, time.UTC)
}

// DateMin return 1970-03-12 (utc)
func DateMin() time.Time {
	return time.Date(1970, 3, 12, 0, 0, 0, 0, time.UTC)
}

// SQLtringToDate parses an SQL date (yyyy-MM-dd) to Time
func SQLtringToDate(sqldateStr string) (time.Time, error) {
	date, err := time.Parse("2006-01-02", sqldateStr)
	if err != nil {
		return date, err
	}
	return date, nil
}

// ConvertDateFormat return golang date format based on on combinations yyyy-MM-dd or dd.MM.YYYY
func ConvertDateFormat(input string) string {
	formats := map[string]string{
		"yyyy": "2006",
		"MM":   "01",
		"dd":   "02"}

	for k, v := range formats {
		input = strings.Replace(input, k, v, 1)
	}
	return input
}

// StringToDateNormal input standard date formats ( dd.MM.yyyy, yyyy-MM-dd, etc) not golang formats
func StringToDateNormal(dateStr string, format string) (time.Time, error) {
	format = ConvertDateFormat(format)
	date, err := time.Parse(format, dateStr)
	if err != nil {
		return date, err
	}
	return date, nil
}

// StringToDate input GO standard date formats
func StringToDate(dateStr string, format string) (time.Time, error) {
	date, err := time.Parse(format, dateStr)
	if err != nil {
		return date, err
	}
	return date, nil
}

// DateToString returns a string representation of in the format 2006-01-02 15:12:12"
func DateToString(t time.Time) string {
	return t.Format(timeFormat)
}

//DateToSQLDateString parses an date to a string in this yyyy-MM-dd format (and remove time part)
func DateToSQLDateString(myDate time.Time) string {
	return myDate.Format("2006-01-02")
}

// DateToSQLDateTimeString returns a string representation of in the format 2006-01-02 15:12:12.123"
func DateToSQLDateTimeString(t time.Time) string {
	return t.Format(timeFormatSQLDateTime)
}

// NowToStringMs returns a string representation of in the format 2006-01-02 15:12:12.123"
func NowToStringMs() string {
	return time.Now().Format(timeFormatSQLDateTime)
}

// NowToString returns a string representation of in the format 2006-01-02 15:12:12"
func NowToString() string {
	return time.Now().Format(timeFormat)
}

// DateRemoveHoursMinutes 1970-03-12 12:33 become 1970-03-12 00:00 (UTC)
func DateRemoveHoursMinutes(d time.Time) time.Time {
	return time.Date(d.Year(), d.Month(), d.Day(), 0, 0, 0, 0, time.UTC)
}

// DateAddDay add a day to date, and remove hours -> 1970-03-12 12:33 become 1970-03-13 00:00 (UTC)
func DateAddDay(d time.Time, days int) time.Time {
	return DateRemoveHoursMinutes(d).AddDate(0, 0, days)
}

// NewDate creates a new date (utc)
func NewDate(year int, month time.Month, day int) time.Time {
	return time.Date(year, month, day, 0, 0, 0, 0, time.UTC)
}
