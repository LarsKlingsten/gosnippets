package gosnippets

import (
	"errors"
	"io/ioutil"
	"os"
	"runtime"
	"time"

	"bitbucket.org/larsklingsten/gosnippets/models"
)

var ramdisk = UserHomeDir() + "/ramdisk/"

// FileWrite save a file to disk, if the file does not exist is created, if exists its overwritten
func FileWrite(fileAndpath string, bytes []byte) error {
	return ioutil.WriteFile(fileAndpath, bytes, 0644)
}

// FileRead opens a file and reads content
func FileRead(fileAndpath string) ([]byte, error) {
	file, err := os.Open(fileAndpath) // For read access.
	if err != nil {
		return nil, err
	}
	defer file.Close()

	fileStats, err := file.Stat()
	if err != nil {
		return nil, err
	}

	data := make([]byte, fileStats.Size())
	count, err := file.Read(data)
	if err != nil {
		return nil, err
	}
	return data[:count], nil
}

// FileReadAndStats opens a file and reads content, return content and stats
func FileReadAndStats(filepath string) (models.FileAndStats, error) {

	file, err := os.Open(filepath) // For read access.
	if err != nil {
		return models.FileAndStats{}, err
	}

	fileStats, err := file.Stat()
	if err != nil {
		return models.FileAndStats{}, err
	}

	data := make([]byte, fileStats.Size())
	count, err := file.Read(data)
	if err != nil {
		return models.FileAndStats{}, err
	}

	fileAndStats := models.FileAndStats{
		Filepath:     filepath,
		Name:         fileStats.Name(),
		Content:      data[:count],
		Size:         fileStats.Size(),
		Modified:     fileStats.ModTime(),
		ReadFromDisk: time.Now(),
		FileNotFound: false,
	}

	return fileAndStats, nil
}

// FileAppend save a file to disk, if the file does not exist is created, content is added to the files (untested)
func FileAppend(fileAndpath string, bytes []byte) error {

	f, err := os.OpenFile(fileAndpath, os.O_CREATE|os.O_APPEND|os.O_WRONLY, 0600)
	if err != nil {
		return err
	}
	defer f.Close()
	if _, err = f.Write(bytes); err != nil {
		return err
	}
	return nil
}

// FileModifyAsExecutable sets the file as executable,writable & readible (chmod 777)
func FileModifyAsExecutable(fileAndpath string) error {
	return os.Chmod(fileAndpath, 0777)
}

// // FileExist2 check whether a file exist FileExist (see below ) may not work as advertised
// func FileExist2(f string) bool {
// 	_, err := os.Stat(f)
// 	if os.IsNotExist(err) {
// 		return false
// 	}
// 	return err == nil
// }

// FileExist check whether a file  exist
func FileExist(fileAndpath string) bool {
	_, err := os.Stat(fileAndpath)
	return err == nil
}

// DirectoryExist check whether a directory exist
func DirectoryExist(path string) bool {
	if stat, err := os.Stat(path); err == nil && stat.IsDir() {
		return true
	}
	return false
}

// DirectoryCreateIfNotExist check if directory exists, if not -> creates all necessary directories
// similar as DirectoryExist(x) followed by DirectoryCreate()
func DirectoryCreateIfNotExist(folderPath string) error {
	if !DirectoryExist(folderPath) {
		return DirectoryCreate(folderPath)
	}
	return nil // already exists
}

// DirectoryCreate create new directory plus parent directories
func DirectoryCreate(path string) error {
	return os.MkdirAll(path, os.ModePerm)
}

// FileDelete check whether a file  exist
func FileDelete(fileAndpath string) bool {
	_, err := os.Stat(fileAndpath)
	return err == nil
}

// UserHomeDir Get the users home directory - should be OK for both windows and  OSX/Linux (Not Tested)
// see http://stackoverflow.com/questions/7922270/obtain-users-home-directory
func UserHomeDir() string {
	if runtime.GOOS == "windows" {
		home := os.Getenv("HOMEDRIVE") + os.Getenv("HOMEPATH")
		if home == "" {
			home = os.Getenv("USERPROFILE")
		}
		return home
	}
	return os.Getenv("HOME")
}

//RamdiskDirectory return the location of where Ramdisk is created
func RamdiskDirectory() string {
	return ramdisk
}

// RamdiskMount mounts a ramdisk at /home/{user}/ramdisk
// example size 512M 1G 4G etc
// may require user to run program as 'sudo'!
func RamdiskMount(size string) (string, error) {

	if !FileExist(RamdiskDirectory()) {
		msg, err := CmdShellCommand(f("mkdir %s", ramdisk))
		if err != nil {
			return "", errors.New(f("Ramdisk:Create Folder msg=%s err=%s", msg, err.Error()))
		}

		cmd := f("sudo mount -t tmpfs -o size=%s,mode=777 tmpf %s", size, RamdiskDirectory())

		msg, err = CmdShellCommand(cmd)
		if err != nil {
			return "", errors.New(f("Ramdisk:Mount msg=%s err=%s", msg, err.Error()))
		}
		return "Ramdisk successfully mounted", nil
	}

	return "Ramdisk was already mounted", nil
}

//RamdiskUnmount unmounts and remove a ramdisk at /home/{user}/ramdisk
func RamdiskUnmount() (string, error) {

	if FileExist(RamdiskDirectory()) {
		cmd := f("sudo umount -l %s", RamdiskDirectory())

		msg, err := CmdShellCommand(cmd)
		if err != nil {
			return "", errors.New(f("Ramdisk:UnMount  err=%s msg=%s", err.Error(), msg))
		}
		msg, err = CmdShellCommand(f("rm -rf  %s", RamdiskDirectory()))
		if err != nil {
			return "", errors.New(f("Ramdisk:rm -rf. err=%s msg=%s", err.Error(), msg))
		}
		return "Ramdisk successfully unmounted", nil
	}
	return "Ramdisk was already unmounted", nil
}

// FileSpeed  test the speed
// sn.FileSpeedTest([]string{"./testfile2.txt"}, false)
func FileSpeed(files []string, doDeleteFiles bool) string {
	TimerStart("FileSpeedTest")

	const fileLength = 100
	var raw [fileLength]byte
	content := raw[:]

	for i := 0; i < fileLength; i++ {
		content[i] = '0'
	}
	content[0] = 's'
	content[fileLength-1] = 'e'

	for _, file := range files {

		TimerStart(file)
		err := FileWrite(file, content)

		if err != nil {
			duration, _ := TimerEnd(file)
			Log(f("%s. %.5f ms", err.Error(), float64(duration)/1000000))
			continue
		}

		if !FileExist(file) {
			duration, _ := TimerEnd(file)
			Log(f("Failed: Does Not Exist Could not create file. %s ms", file, float64(duration)/1000000))
			continue
		}

		if doDeleteFiles {
			err = os.Remove(file)
			if err == nil {
				Log(f("Success: %s", TimerEndToStr(file)))
			} else {
				Log(f("Failed to Delete:  %s", TimerEndToStr(file)))
			}
		}
	}
	return TimerEndToStr("FileSpeedTest")
}

//// WALK DIRECTORY ////

// WalkDirectory walks the directory and stores the result service.files (pointer)
func WalkDirectory(dir string, isRecursive bool) ([]models.FileAndStats, error) {
	var files []models.FileAndStats
	err := walkDirectory(dir, &files, isRecursive)
	if err != nil {
		return nil, err
	}
	return files, nil
}

// WalkDirectory walks recursively a directory and returns a models.FileInfo array
// uses by Directory() above
func walkDirectory(root string, filesArray *[]models.FileAndStats, isRecursive bool) error {
	files, err := ioutil.ReadDir(root)
	if err != nil {
		return err
	}
	for _, file := range files {
		if file.IsDir() {
			if isRecursive {
				// sn.Log(f("path: %s" , root+"/"+file.Name()))
				walkDirectory(root+"/"+file.Name(), filesArray, isRecursive)
			}
		} else {
			*filesArray = append(*filesArray, ConvertOsFileinfoToModelsFile(file, root))
		}
	}
	return nil
}

// ConvertOsFileinfoToModelsFile convert a os.FileInfo object to File
func ConvertOsFileinfoToModelsFile(fileInfo os.FileInfo, dir string) models.FileAndStats {
	return models.FileAndStats{
		Filepath:     dir + "/" + fileInfo.Name(),
		Name:         fileInfo.Name(),
		Size:         fileInfo.Size(),
		Modified:     fileInfo.ModTime(),
		ReadFromDisk: time.Now(),
	}
}
