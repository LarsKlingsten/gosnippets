package gosnippets

import (
	"math"
)

// RoundWithPrecision rounds up or down with precision (set the round decimals)
// note: precision must be 0 or larger
func RoundWithPrecision(f float64, precision int) float64 {
	if precision < 0 {
		return f
	}
	shift := math.Pow(10, float64(precision))
	return Round(f*shift) / shift
}

// Round up or down to nearest integer
// note: 0.499999999999999 will be round down (as expected to 0)
//       0.4999999999999999 will be round up (unexpected to 1)
func Round(f float64) float64 {
	if f < 0 {
		return math.Ceil(f - 0.5)
	}
	return math.Floor(f + 0.5)
}

// Abs64 the absolut number of of an int64
// max=MaxInt64, min=MinInt64 - 1
func Abs64(number int64) int64 {
	if number < 0 {
		return -number
	}
	return number
}

// Abs the absolut number of  an integer (untested)
func Abs(number int) int {
	if number < 0 {
		return -number
	}
	return number
}
