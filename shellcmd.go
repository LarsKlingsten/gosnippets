package gosnippets

import (
	"bytes"
	"compress/bzip2"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"runtime"
	"syscall"
)

// Execute command and return exited code.
func execcmd(cmd *exec.Cmd) (string, error) {

	msg := f("cmd=%s ", cmd.Args)
	var waitStatus syscall.WaitStatus

	err := cmd.Run()
	if err != nil {
		msg += f("Err=%s", err)
		if exitError, ok := err.(*exec.ExitError); ok {
			waitStatus = exitError.Sys().(syscall.WaitStatus)
			msg += f("(exit code: %s) ", []byte(fmt.Sprintf("%d", waitStatus.ExitStatus())))
		}
	} else {
		waitStatus = cmd.ProcessState.Sys().(syscall.WaitStatus)
		msg += f("(exit code: %s) ", []byte(fmt.Sprintf("%d", waitStatus.ExitStatus())))
	}

	return msg, err
}

// CmdShellCommand executes a external terminal command
// Deprecated - use "Cmd" instead
// example: msg, err := shellCommand("mkdir /home/larsk/myNewDirectory")
func CmdShellCommand(command string) (string, error) {

	fmt.Println("FATAL! @Klingsten.Snippets.CmdShellCommand is Deprecated - use 'Cmd' instead")
	fmt.Println("FATAL! @Klingsten.Snippets.CmdShellCommand is terminating your program")
	os.Exit(1)

	// out, err := exec.Command("sh", "-c", command).CombinedOutput()
	return "", nil
}

// CmdKillPort closes a port
func CmdKillPort(port int) (string, error) {
	if runtime.GOOS == "windows" {
		command := fmt.Sprintf("(Get-NetTCPConnection -LocalPort %d).OwningProcess -Force", port)
		return execcmd(exec.Command("Stop-Process", "-Id", command))
	}
	command := fmt.Sprintf("lsof -i tcp:%d | grep LISTEN | awk '{print $2}' | xargs kill -9", port)
	return execcmd(exec.Command("bash", "-c", command))

}

// CmdBash executes a bash script and return output
// usage result, err := sn.CmdBash("myOwnBashScript.sh", "args0", "args1" )
func CmdBash(cmd ...string) (string, error) {

	out, err := exec.Command("bash", cmd...).Output()

	if err != nil {
		return string(out), errors.New(f("Err @CmdBash cmd=%s bash=%s err=%s", cmd, out, err.Error()))
	}
	return string(out), nil
}

// Cmd run shell command (ls -lah)
func Cmd(command string, args ...string) ([]byte, error) {
	cmd := exec.Command(command, args...)
	var stdout, stderr bytes.Buffer
	cmd.Stdout = &stdout
	cmd.Stderr = &stderr
	err := cmd.Run()
	if err != nil {
		return nil, err
	}

	if len(stderr.Bytes()) > 0 {
		return nil, errors.New(stderr.String())
	}
	return stdout.Bytes(), nil
}

// CmdLs list current directory (ls -lah)
// also example of how to use "Cmd" above
func CmdLs() ([]byte, error) {
	return Cmd("ls", "-lah")
}

// StandardOut redirect shell output to golang
func StandardOut() {

	cmd := exec.Command("sh", "-c", "echo stdout; echo 1>&2 stderr")
	stderr, err := cmd.StderrPipe()
	if err != nil {
		log.Fatal(err)
	}

	if err := cmd.Start(); err != nil {
		log.Fatal(err)
	}

	slurp, _ := ioutil.ReadAll(stderr)
	fmt.Printf("%s\n", slurp)

	if err := cmd.Wait(); err != nil {
		log.Fatal(err)
	}

}

// CmdDiff saves a patch file
func CmdDiff(sourceFile, backupFile, patchFile string) error {

	cmd := exec.Command("diff", sourceFile, backupFile)

	stdout, err := cmd.StdoutPipe()
	if err != nil {
		return err
	}
	if err := cmd.Start(); err != nil {
		return err
	}
	buf := new(bytes.Buffer)

	//cmd := exec.Command("tr", "a-z", "A-Z")

	buf.ReadFrom(stdout)

	//fmt.Println("'" + buf.String() + "'")

	err = FileWrite(patchFile, buf.Bytes())
	if err != nil {
		return err
	}

	return nil
}

// CmdDiff2 saves a patch file
func CmdDiff2(sourceFile, backupFile, patchFile string) error {

	path, err := exec.LookPath("diff") // find the diff command
	if err != nil {
		return err
	}
	var command = fmt.Sprintf("  '%s'  '%s'  > '%s'", sourceFile, backupFile, patchFile)

	out, err := exec.Command(path, command).Output()
	// out, err := exec.Command("diff").Output()
	if err != nil {
		return err
	}
	fmt.Printf("The date is %s\n", out)

	return nil

}

// PrintDir print current directory (ls -lah)
func PrintDir() {
	str, err := CmdLs()

	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(str)
}

// Cmddiff3 list current directory (ls -lah)
func Cmddiff3(sourceFile, backupFile, patchFile string) (string, error) {
	cmd := exec.Command("diff", sourceFile, backupFile)
	var stdout, stderr bytes.Buffer
	cmd.Stdout = &stdout
	cmd.Stderr = &stderr
	err := cmd.Run()
	if err != nil {
		return "", err
	}

	if len(stderr.Bytes()) > 0 {
		return "", errors.New(stderr.String())
	}
	return stdout.String(), nil
}

// CmdStandardOut2 redirect shell output to golang
func CmdStandardOut2() {
	cmd := exec.Command("echo", "-n", `{"Name": "Bob", "Age": 32}`)
	stdout, err := cmd.StdoutPipe()
	if err != nil {
		log.Fatal(err)
	}
	if err := cmd.Start(); err != nil {
		log.Fatal(err)
	}
	buf := new(bytes.Buffer)
	buf.ReadFrom(stdout)

	fmt.Println(buf.String())
}

// CmdbZipCompress compressed a []byte to with bZip
// https://blog.kowalczyk.info/article/wOYk/advanced-command-execution-in-go-with-osexec.html
// https://github.com/kjk/go-cookbook/blob/master/advanced-exec/06-feed-stdin.go
// compress data using bzip2 without creating temporary files
func CmdbZipCompress(d []byte) ([]byte, error) {
	var out bytes.Buffer
	cmd := exec.Command("bzip2", "-c", "-9")
	cmd.Stdin = bytes.NewBuffer(d)
	cmd.Stdout = &out
	err := cmd.Run()
	if err != nil {
		return nil, err
	}
	return out.Bytes(), nil
}

func bzipDecompress(d []byte) ([]byte, error) {
	r := bzip2.NewReader(bytes.NewBuffer(d))
	var out bytes.Buffer
	_, err := io.Copy(&out, r)
	if err != nil {
		return nil, err
	}
	return out.Bytes(), nil
}
