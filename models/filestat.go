package models

import "time"

// FileAndStats contains file content and stats
type FileAndStats struct {
	Filepath     string
	Content      []byte
	Name         string
	Size         int64
	Modified     time.Time
	ReadFromDisk time.Time
	FileNotFound bool // set to true if an attempt has been made to load file, and it failed
}

// HasContent return whether the file has content or not
func (file *FileAndStats) HasContent() bool {
	if file.Content == nil || len(file.Content) == 0 {
		return false
	}
	return true
}
