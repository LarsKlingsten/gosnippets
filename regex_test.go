package gosnippets

import (
	"testing"
)

func TestIsDateRFC3339(t *testing.T) {
	type args struct {
		s string
	}
	tests := []struct {
		name string
		args args
		want bool
	}{

		{"OK date", args{"2016-05-31T13:24:13.960272+02:00"}, true},
		{"OK date", args{"2016-05-21T13:24:13.960272+02:00"}, true},
		{"OK date", args{"2016-05-01T13:24:13.960272+02:00"}, true},
		{"no 0's", args{"2016-5-1T13:24:13.960272+02:00"}, true},
		{"Bad Date", args{"2016-99-31T13:24:13.960272+02:00"}, false},
		{"Bad Date", args{"2016- 99 -31T13:24:13.960272+02:00"}, false},
		{"Bad Date", args{"2016-05-33T13:24:13.960272+02:00"}, false},
		{"str", args{"string"}, false},
		{"emptry str", args{""}, false},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := IsDateRFC3339(tt.args.s); got != tt.want {
				t.Errorf("IsRFC3339() = name=%s input=%v %v, want %v", tt.name, tt.args.s, got, tt.want)
			}
		})
	}
	LogTestWithFuncName(len(tests))
}

func TestRegexAllowedChars(t *testing.T) {

	type testThis struct {
		str   string
		regex string

		expect string
	}

	const reqSQL = "[^a-zA-Z0-9@=(),._]+"

	tests := []testThis{
		{"lars@@klingsten", reqSQL, "lars@@klingsten"},
		{"<larsk<@", reqSQL, "larsk@"},
		{"<lars>", reqSQL, "lars"},
	}

	for _, test := range tests {
		result := RegexAllowedChars(test.str, test.regex)
		if result != test.expect {
			t.Errorf("@TestStringRenoveCharsRegex Expected: '%v' -> got: '%v' ", test.expect, result)
		}
	}

	LogTestWithFuncName(len(tests))
}

func TestRegexDelimitor(t *testing.T) {

	type testThis struct {
		count  int
		str    string
		expect []string
	}

	// const reqSQL = "[^a-zA-Z0-9@=(),._]+"

	tests := []testThis{
		{0, "", []string{}},
		{1, `comma`, []string{"comma"}},
		{2, `comma,quote`, []string{"comma", "quote"}},
		{3, `comma;quote`, []string{"comma", "quote"}},
		{4, `comma;quote,test`, []string{"comma", "quote", "test"}}, // ; and ,
		{5, `"doublequote"`, []string{"doublequote"}},
		{6, `'singlequote'`, []string{"singlequote"}},                                                   // "// "
		{7, `'singlequote','singlequote2'`, []string{"singlequote", "singlequote2"}},                    // "
		{8, `"doublequote1","doublequote2"`, []string{"doublequote1", "doublequote2"}},                  // "
		{9, `"1double,quote1","1double,quote2"`, []string{"1double,quote1", "1double,quote2"}},          // "
		{10, `'singlequote',"1double,quote2"`, []string{"singlequote", "1double,quote2"}},               // "
		{11, `'<lars@klingsten>' `, []string{"lars@klingsten"}},                                         // "
		{12, `'@klingsten>' `, []string{"@klingsten"}},                                                  // "
		{13, `'lars_klin-1gsetn@kli12ngsten.com.sg' `, []string{"lars_klin-1gsetn@kli12ngsten.com.sg"}}, // "

	}

	for _, test := range tests {
		result := RegexDelimitor(test.str)
		for i := range result {
			if result[i] != test.expect[i] {
				t.Errorf("@TestStringRenoveCharsRegex %d Expected: '%v' -> got: '%v' ", test.count, test.expect, result)
			}
		}
	}
	LogTestWithFuncName(len(tests))
}
