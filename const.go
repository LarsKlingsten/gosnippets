package gosnippets

import "fmt"

// DefaultNewline is the end-of-line delimitor
const DefaultNewline = "\n"

// DefaultTab is the tabulator char
const DefaultTab = "\t"

var f = fmt.Sprintf
var p = fmt.Println

// MaxUint Max unsigned integer (=0)
const MaxUint = ^uint(0)

// MinUint min unsigned integer
const MinUint = 0

// MaxInt max signed integer
const MaxInt = int(MaxUint >> 1)

// MinInt min signed integer
const MinInt = -MaxInt - 1

// MaxUint64 max unsigned integer
const MaxUint64 = ^uint64(0)

// MinUint64 min unsigned integer
const MinUint64 = int64(0)

// MaxInt64 min usigned integer (=0)
const MaxInt64 = int64(MaxUint64 >> 1)

// MinInt64 min signed 64-integer  (=0)
const MinInt64 = -MaxInt64 - 1
