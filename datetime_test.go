package gosnippets

import (
	"errors"
	"reflect"
	"strings"
	"testing"
	"time"
)

func TestConvertDateFormat(t *testing.T) {

	type testThis struct {
		in  string
		out string
	}

	var tests = []testThis{
		{"yyyy-MM-dd", "2006-01-02"},
		{"dd.MM.yyyy", "02.01.2006"},
		{"MM/dd/yyyy", "01/02/2006"},
	}
	for _, test := range tests {
		out := ConvertDateFormat(test.in) // access
		if out != test.out {              // assess result
			t.Errorf("@Snippets.DateTest(''%v'') == %v  (expected: '%v')", test.in, test.out, out)
		}
	}

	LogTestWithFuncName(len(tests))
}

func TestSnippetsStringToDate(t *testing.T) {

	type testThis struct {
		dateStr      string
		dateFormat   string
		expectedDate time.Time
		expectedErr  error
	}
	var tests = []testThis{
		{"1970-03-12", "yyyy-MM-dd", time.Date(1970, 03, 12, 0, 0, 0, 0, time.UTC), nil},
		{"1970-12-03", "yyyy-dd-MM", time.Date(1970, 03, 12, 0, 0, 0, 0, time.UTC), nil},
		{"12.03.1970", "dd.MM.yyyy", time.Date(1970, 03, 12, 0, 0, 0, 0, time.UTC), nil},
		{"19700312", "yyyyMMdd", time.Date(1970, 03, 12, 0, 0, 0, 0, time.UTC), nil},

		{"1970-12-31", "yyyy-MM-dd", time.Date(1970, 12, 31, 0, 0, 0, 0, time.UTC), nil},
		{"1970-31-12", "yyyy-dd-MM", time.Date(1970, 12, 31, 0, 0, 0, 0, time.UTC), nil},
		{"31.12.1970", "dd.MM.yyyy", time.Date(1970, 12, 31, 0, 0, 0, 0, time.UTC), nil},
		{"19701231", "yyyyMMdd", time.Date(1970, 12, 31, 0, 0, 0, 0, time.UTC), nil},

		{"1970-31-13", "yyyy-dd-MM", time.Date(1, 1, 1, 0, 0, 0, 0, time.UTC), nil}, // should include an error
		{"00.01.1970", "dd.MM.yyyy", time.Date(1, 1, 1, 0, 0, 0, 0, time.UTC), nil}, // fixed  https://github.com/golang/go/issues/17874
		{"32.01.1970", "dd.MM.yyyy", time.Date(1, 1, 1, 0, 0, 0, 0, time.UTC), errors.New("day out of range")},
	}
	for _, dt := range tests {
		out, err := StringToDateNormal(dt.dateStr, dt.dateFormat) // access
		if !out.Equal(dt.expectedDate) {                          // assess result
			t.Errorf("out: Expected: '%v' -> was: '%v' (Snippets.StringToDate(%v, %v)", dt.expectedDate, out, dt.dateStr, dt.dateFormat)
		}

		if err != nil && dt.expectedErr != nil && !strings.Contains(err.Error(), dt.expectedErr.Error()) {
			t.Errorf("err: Expected: '%v' -> was: '%v'  func(%v, %v)   ", dt.expectedErr, err, dt.dateStr, dt.dateFormat)
		}
	}

	LogTestWithFuncName(len(tests))
}
func TestDateRemoveHoursMinutes(t *testing.T) {
	type args struct {
		d time.Time
	}

	tests := []struct {
		name string
		args args
		want time.Time
	}{
		{"ok", args{time.Date(1970, 3, 12, 0, 1, 12, 3, time.UTC)}, time.Date(1970, 3, 12, 0, 0, 0, 0, time.UTC)},
		{"nodiff", args{time.Date(2017, 12, 30, 0, 0, 0, 0, time.UTC)}, time.Date(2017, 12, 30, 0, 0, 0, 0, time.UTC)},
		{"bad dates", args{time.Date(1017, 30, 30, 0, 1, 0, 1, time.UTC)}, time.Date(1017, 30, 30, 0, 0, 0, 0, time.UTC)},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := DateRemoveHoursMinutes(tt.args.d); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("DateRemoveHoursMinutes() %s = %v, want %v", tt.name, got, tt.want)
			}
		})
	}
	LogTestWithFuncName(len(tests))
}

func TestNewDate(t *testing.T) {
	type args struct {
		year  int
		month time.Month
		day   int
	}
	tests := []struct {
		name string
		args args
		want time.Time
	}{
		{"ok", args{2017, time.March, 12}, time.Date(2017, 3, 12, 0, 0, 0, 0, time.UTC)},
		{"bad date, day=32", args{2017, time.March, 32}, time.Date(2017, 3, 32, 0, 0, 0, 0, time.UTC)},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewDate(tt.args.year, tt.args.month, tt.args.day); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewDate() = %v, want %v", got, tt.want)
			}
		})
	}
	LogTestWithFuncName(len(tests))

}

func TestDateAddDay(t *testing.T) {
	type args struct {
		d    time.Time
		days int
	}
	tests := []struct {
		name string
		args args
		want time.Time
	}{
		{"without hours/mins", args{time.Date(2017, 3, 12, 0, 0, 0, 0, time.UTC), 10}, time.Date(2017, 3, 22, 0, 0, 0, 0, time.UTC)},
		{"with    hours/mins", args{time.Date(2017, 3, 12, 2, 3, 4, 5, time.UTC), 10}, time.Date(2017, 3, 22, 0, 0, 0, 0, time.UTC)},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := DateAddDay(tt.args.d, tt.args.days); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("DateAddDay() = %v, want %v", got, tt.want)
			}
		})
	}

	LogTestWithFuncName(len(tests))

}
