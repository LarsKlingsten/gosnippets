package gosnippets

import (
	"regexp"
)

// regexRFC3339 - mathing "2015-01-23T" -> not the full string
const regexDateRFC3339 = `((\d{4})\-(0?[1-9]|1[012])\-(0?[1-9]|[12][0-9]|3[01])(T|t))`

// IsDateRFC3339 check whether string is LIKELY a RFC3339 time format ()
// note we only match first 10 chars of the RFC "2015-01-23T" and not the full strring
func IsDateRFC3339(s string) bool {
	matched, err := regexp.MatchString(regexDateRFC3339, s)
	if err != nil {
		return false
	}
	return matched
}

// RegexAllowedChars remove chars not inclubed in regex string ex [^a-zA-Z0-9 =<>?*(),._]+
func RegexAllowedChars(s, regex string) string {
	req, _ := regexp.Compile(regex)
	return req.ReplaceAllString(s, "")
}

// RegexFindSubmatch return []string for s matching regex
// we are getting the last value (except "")  possible of the sub matches
func RegexFindSubmatch(s string, regex string) []string {
	var outArr []string
	re := regexp.MustCompile(regex)
	r := re.FindAllStringSubmatch(s, -1)
	var curValue string
	for _, array1 := range r {
		for _, array2 := range array1 {
			if array2 != "" {
				curValue = array2
			}
		}
		if curValue != "" {
			outArr = append(outArr, curValue)
		}
	}
	return outArr
}

// RegexDelimitor return []string delimited by ",", ";" or single or double quotes
// removes double and single quote as well
func RegexDelimitor(s string) []string {
	const delimitor = `("(\w[^"]*)"|'(\w[^']*)'|([\w@][^"'<>;,]*))`
	return RegexFindSubmatch(s, delimitor)
}

// RegexGolangTemplateFields return []string matching '{{ anything }}'
// this includes also {{if .Field }}
func RegexGolangTemplateFields(s string) []string {
	const golangTemplateFields = `\{\{(.*?)}}`
	return RegexFindSubmatch(s, golangTemplateFields)
}
