package gosnippets

import (
	"bytes"
	"testing"
)

func TestDir(t *testing.T) {

	const countTests = 2
	dirStr, err := CmdLs()

	if err != nil {
		t.Errorf("@TestDir. Err=%v", err)
	}

	if len(dirStr) < 10 {
		t.Errorf("@TestDir. Nothing was output=%d bytes", len(dirStr))
	}

	LogTestWithFuncName(countTests)
}

func TestCmd(t *testing.T) {

	const countTests = 1
	dirStr, err := Cmd("ls", "-lah", "/home/larsk") // ls -lah /home/larsk

	if err != nil {
		t.Errorf("@TestDir. Err=%v", err)
	}

	LogFuncname(f("Cmd -> dirStr=%d bytes tests=%d", len(dirStr), countTests))
}

//TestCompress Test diff
func TestCompress(t *testing.T) {

	countTests := 4

	// const source = "/home/larsk/shared/source/"
	// const backup = "/home/larsk/shared/backup/"
	// const patch = "/home/larsk/shared/patch/"

	file, err := FileRead("./debug.test")

	if err != nil {
		t.Errorf("FileRead failed with %s\n", err)
		return
	}

	compressed, err := CmdbZipCompress(file)
	if err != nil {
		t.Errorf("bzipCompress() failed with %s\n", err)
	}

	//	FileWrite("testFiles/debug_compressed", compressed)

	// verify compression was correct by decompressing and comparing
	// with the original
	uncompressed, err := bzipDecompress(compressed)

	if err != nil {
		t.Errorf("bzipDecompress() failed with %s\n", err)
	}
	if !bytes.Equal(file, uncompressed) {
		t.Errorf("decompressed data not the same as original data\n")
	}

	LogTestWithFuncName(countTests)
	//LogFuncname(f("Compressed %d bytes to %d, saving %d\n", len(file), len(compressed), len(file)-len(compressed)))

}
