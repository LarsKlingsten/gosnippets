package gosnippets

import (
	"io/ioutil"
	"net"
	"net/http"
	"strings"
)

// GetLocalIP returns the non loopback local IP of the host -> http://stackoverflow.com/questions/23558425/how-do-i-get-the-local-ip-address-in-go
func GetLocalIP() string {
	addrs, err := net.InterfaceAddrs()
	if err != nil {
		return ""
	}
	for _, address := range addrs {
		// check the address type and if it is not a loopback the display it
		if ipnet, ok := address.(*net.IPNet); ok && !ipnet.IP.IsLoopback() {
			if ipnet.IP.To4() != nil {
				return ipnet.IP.String()
			}
		}
	}
	return ""
}

// HTTPRequest standard url request
// method -> "PUT", "POST", "GET", or "DELETE"
// body   -> is the raw text passed to "PUT" AND "POST" request
func HTTPRequest(method string, url string, body []byte) ([]byte, error) {

	client := &http.Client{}
	request, err := http.NewRequest(method, url, strings.NewReader(string(body)))
	if err != nil {
		return nil, err
	}

	response, err := client.Do(request)
	if err != nil {
		return nil, err
	}

	defer response.Body.Close()
	contents, err := ioutil.ReadAll(response.Body)

	if err != nil {
		return nil, err
	}

	return contents, nil
}
