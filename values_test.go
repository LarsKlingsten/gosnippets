package gosnippets

import (
	"testing"
	"time"
)

func TestIsString(t *testing.T) {
	type testThis struct {
		value  interface{}
		expect bool
	}

	tests := []testThis{
		{"larsklingsten", true},
		{"", true},
		{``, true},
		{1, false},
		{time.Now(), false},
		{nil, false},
	}

	for _, test := range tests {
		result := IsString(test.value)
		if result != test.expect {
			t.Errorf("@TestIsString Expected: '%v' -> got: '%v' ", test.expect, result)
		}
	}

	LogTestWithFuncName(len(tests))
}
