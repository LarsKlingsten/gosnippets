package gosnippets

import (
	"errors"
	"time"
)

var timings = make(map[string]time.Time)

// TimerStart starts a new Timer
func TimerStart(name string) {
	timings[name] = time.Now()
}

//TimerEnd stops the timer return result in nanoseconds
// if timer name not found -1 is returned
func TimerEnd(name string) (time.Duration, error) {
	start, isFound := timings[name]
	if !isFound {
		return -1, errors.New(f("Err. TimerEndToLog('%s')  called before TimerStart()", name))
	}

	delete(timings, name)
	return time.Since(start), nil
}

//TimerEndToLog stops and prints duration in milisec
func TimerEndToLog(name string) {
	Log(TimerEndToStr(name))
}

//TimerEndToStr stops and and return duration in milisec as a string
func TimerEndToStr(name string) string {
	duration, err := TimerEnd(name)
	if err != nil {
		return f("Err. TimerEndToLog('%s')  called before TimerStart()", name)
	}
	milisec := float64(duration.Nanoseconds()) / 1000000
	return f("completed '%s' in %.5f ms", name, milisec)
}

// TimerSleep waits
func TimerSleep(dur int) {
	time.Sleep(time.Duration(dur) * time.Millisecond)
}
