package gosnippets

import (
	"testing"
)

func TestRoundWithPrecision(t *testing.T) {

	type testThis struct {
		number    float64
		precision int
		expected  float64
	}

	tests := []testThis{
		{number: 1.055, precision: 2, expected: 1.06},
		{number: 1.054, precision: 2, expected: 1.05},
		{number: 1.05001, precision: 2, expected: 1.05},
		{number: 1.044, precision: 2, expected: 1.04},
		{number: 1.0445, precision: 2, expected: 1.04},
		{number: 0, precision: 2, expected: 0},

		{number: -1.055, precision: 2, expected: -1.06},
		{number: -1.054, precision: 2, expected: -1.05},
		{number: -1.05001, precision: 2, expected: -1.05},
		{number: -1.044, precision: 2, expected: -1.04},
		{number: -1.0445, precision: 2, expected: -1.04},
		{number: -0, precision: 2, expected: 0},

		{number: 1.123456789, precision: 5, expected: 1.12346},
		{number: 1.123456789, precision: 0, expected: 1},
		{number: 1.123456789, precision: -2, expected: 1.123456789},
	}

	for _, test := range tests {
		result := RoundWithPrecision(test.number, test.precision)
		if result != test.expected {
			t.Errorf("@test RoundWithPrecision in=(%v,%v) Expected: '%v' -> got: '%v' ", test.number, test.precision, test.expected, result)
		}
	}

	LogTestWithFuncName(len(tests))
}

func TestRound(t *testing.T) {

	type testThis struct {
		number   float64
		expected float64
	}

	tests := []testThis{
		{number: 1.0, expected: 1.0},
		{number: 1.5, expected: 2.0},
		{number: 0.5000000000000001, expected: 1.0},
		{number: 0.499999999999999, expected: 0.0},
		{number: -1.0, expected: -1.0},
		{number: -1.4999, expected: -1.0},
		{number: -1.5, expected: -2.0},
		{number: -0, expected: 0},
		{number: 0, expected: 0},
	}

	for _, test := range tests {
		result := Round(test.number)
		if result != test.expected {
			t.Errorf("@TestRound Expected: '%v' -> got: '%v' ", test.expected, result)
		}
	}
	LogTestWithFuncName(len(tests))
}

func TestAbs64(t *testing.T) {

	type testThis struct {
		number   int64
		expected int64
	}

	tests := []testThis{
		{number: 1, expected: 1},
		{number: -1, expected: 1},
		{number: 0, expected: 0},
		{number: MaxInt64, expected: MaxInt64},
		{number: MinInt64 + 1, expected: MaxInt64},
	}

	for _, test := range tests {
		result := Abs64(test.number)
		if result != test.expected {
			t.Errorf("@ TestAbs64 Expected: '%v' -> got: '%v' ", test.expected, result)
		}
	}
	LogTestWithFuncName(len(tests))
}

// benchmarks

func BenchmarkRoundWithPrecision(b *testing.B) {
	for i := 0; i < b.N; i++ {
		RoundWithPrecision(123.456, 2)
	}
}
func BenchmarkAbs64(b *testing.B) {
	for i := 0; i < b.N; i++ {
		Abs64(120)
		Abs64(-666)
	}
}

func BenchmarkAbs(b *testing.B) {
	for i := 0; i < b.N; i++ {
		Abs(120)
		Abs(-666)
	}
}
