package gosnippets

import (
	"errors"
	"reflect"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

type Container interface{}

func TestStringToFloat(t *testing.T) {

	type testThis struct {
		in       string
		ukFormat bool
		expOut   float64
		expErr   error
	}

	// 7,700.00
	var tests = []testThis{
		{"1.12", true, 1.12, nil},
		{"1,234", true, 1234.0, nil},
		{"1,234.56", true, 1234.56, nil},
		{"1,234,567", true, 1234567.0, nil},

		{"2,12", false, 2.12, nil},
		{"2.234", false, 2234.0, nil},
		{"2.234,56", false, 2234.56, nil},
		{"2.234.567", false, 2234567.0, nil},

		{"not an float", false, 0.0, errors.New("not an float")},
		{"3,234,567", false, 0.0, errors.New("invalid syntax")}, // this is using UK format and passing in Non Uk
	}

	for _, dt := range tests {
		actOut, actErr := StringToFloat(dt.in, dt.ukFormat) // access
		if actOut != dt.expOut {                            // assess result
			t.Errorf("@Snippets.DateTest(''%v'') == %f  (expected: %f)", dt.in, actOut, dt.expOut)
		}

		if actErr != dt.expErr && !strings.Contains(actErr.Error(), dt.expErr.Error()) { // assess result
			t.Errorf("@Snippets.DateTest(''%v'') == %v  (expected: %v)", dt.in, actErr, dt.expErr)
		}

	}
	LogTestWithFuncName(len(tests))
}

func TestRemoveNonAlphaNum(t *testing.T) {

	type testThis struct {
		para   string
		expect string
	}

	tests := []testThis{
		{para: "abcABC1234ZZZzzz", expect: "abcABC1234ZZZzzz"},
		{para: "abcABC1234ZZZzzzæøå", expect: "abcABC1234ZZZzzz"},
		{para: "abcABC1234ZZZzzz,.!'#¤%&/(", expect: "abcABC1234ZZZzzz"},
		{para: " abcABC1234ZZZzzz ", expect: "abcABC1234ZZZzzz"},
		{para: " abcABC12 34Z ZZzzz ", expect: "abcABC1234ZZZzzz"},
	}

	for _, test := range tests {
		result := StringRemoveNonAlphaNum(test.para)
		if result != test.expect {
			t.Errorf("@TestRemoveNonAlphaNum Expected: '%v' -> got: '%v' ", test.expect, result)
		}
	}

	LogTestWithFuncName(len(tests))
}

func TestPadInt64(t *testing.T) {

	type testThis struct {
		number  int64
		length  int
		padding string
		expect  string
	}

	tests := []testThis{
		{number: 1, length: 5, padding: "0", expect: "00001"},
		{number: 1, length: -5, padding: "0", expect: "1"},
		{number: 1, length: 5, padding: " ", expect: "    1"},
		{number: 1, length: -5, padding: " ", expect: "1"},
		{number: 1, length: 5, padding: "", expect: "1"},
		{number: -1, length: 5, padding: "0", expect: "-00001"},

		{number: 123456789, length: 12, padding: "0", expect: "000123456789"}, // padding is less than len(string)
		{number: -123456789, length: 12, padding: "0", expect: "-000123456789"},

		{number: 123456789, length: 5, padding: "0", expect: "123456789"}, // padding is less than len(string)
		{number: -123456789, length: 5, padding: "0", expect: "-123456789"},
	}

	for _, test := range tests {
		result := PadInt64(test.number, test.length, test.padding)
		if result != test.expect {
			t.Errorf("@TestPadInt64 Expected: '%v' -> got: '%v' ", test.expect, result)
		}
	}

	LogTestWithFuncName(len(tests))
}

// benchmark

func BenchmarkPadInt64(b *testing.B) {
	for i := 0; i < b.N; i++ {
		PadInt64(123, 2, " ")
	}
}

func TestStringLeft(t *testing.T) {

	type testThis struct {
		str    string
		length int
		expect string
	}

	tests := []testThis{
		{"larsklingsten", 5, "larsk"},
		{"larsk", 5, "larsk"},
		{"lars", 5, "lars"},
	}

	for _, test := range tests {
		result := StringLeft(test.str, test.length)
		if result != test.expect {
			t.Errorf("@TestPadInt64 Expected: '%v' -> got: '%v' ", test.expect, result)
		}
	}

	LogTestWithFuncName(len(tests))
}

func TestStringRight(t *testing.T) {

	type testThis struct {
		str    string
		length int
		expect string
	}

	tests := []testThis{
		{"larsklingsten", 5, "gsten"},
		{"larsk", 5, "larsk"},
		{"lars", 5, "lars"},
	}

	for _, test := range tests {
		result := StringRight(test.str, test.length)
		if result != test.expect {
			t.Errorf("@TestStringRight Expected: '%v' -> got: '%v' ", test.expect, result)
		}
	}

	LogTestWithFuncName(len(tests))
}

func TestStringRemove(t *testing.T) {

	type args struct {
		s []string
		r string
	}
	tests := []struct {
		name string
		args args
		want []string
	}{
		{"1", args{[]string{"one", "two", "three", "four"}, "two"}, []string{"one", "three", "four"}},
		{"2", args{[]string{"one", "", "three", "four"}, "two"}, []string{"one", "", "three", "four"}},
		{"3", args{[]string{"one", "", "three", "four"}, ""}, []string{"one", "three", "four"}},
		{"4", args{[]string{"one", "", "three", "four"}, ""}, []string{"one", "three", "four"}},
		{"5", args{[]string{"1", "three", "four"}, "1"}, []string{"three", "four"}},
		{"6", args{[]string{"1", "1", "three", "four"}, "1"}, []string{"three", "four"}},
		{"7", args{[]string{"1", "1", "three", "four"}, "1"}, []string{"three", "four"}},
		{"8", args{[]string{"1", "1", "1", "1"}, "1"}, []string{}},
		{"9", args{[]string{"0", "1", "2", "1"}, "1"}, []string{"0", "2"}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := StringRemove(tt.args.s, tt.args.r); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("remove() test=%s   got=%v, want=%v", tt.name, got, tt.want)
			}
		})
	}
	LogTestWithFuncName(len(tests))
}

func TestDecodeQuotedPrintableByte(t *testing.T) {

	expect := "Dette er en test fra Århus"
	testBytes := []byte("Dette er en test fra =C3=85rhus")

	for i := range []int{1, 20, 30, 50, 100, 2000} {
		test := []byte(StringRepeat(string(testBytes), i))
		expect := StringRepeat(expect, i)
		result := StringDecodeQuotedPrintableBytes(test)

		if result != expect {
			t.Errorf("@TestStringRight Expected: '%v' -> got: '%v' ", expect, result)
		}

		t.Log(result)

	}

}

func TestStringExtractDomainFromEmailAdr(t *testing.T) {

	tests := []struct {
		email     string
		expDomain string
	}{
		{email: "lars@gmail.com", expDomain: "gmail.com"},
		{email: "lars@gmail.com.sg", expDomain: "gmail.com.sg"},
		{email: "<lars@gmail.com>", expDomain: "gmail.com"},
		{email: "<lars@gmail.com.sg>", expDomain: "gmail.com.sg"},
		{email: "lars Klingsten <lars@gmail.com>", expDomain: "gmail.com"},
		{email: "lars Klingsten <lars@gmail.com.sg>", expDomain: "gmail.com.sg"},
		{email: "lars Klingsten <badema@com>", expDomain: "com"},
	}

	for i, test := range tests {
		got := StringExtractEmailDomain(test.email)
		assert.Equal(t, test.expDomain, got, "index %d", i)

	}
}

func BenchmarkDecodeQuotedPrintableString(b *testing.B) {
	testBytes := []byte("Dette er en test fra =C3=85rhus")

	// number of tests
	for i := 0; i < b.N; i++ {

		// run functions
		for i := range []int{1, 20, 30, 50, 100, 2000} {
			test := StringRepeat(string(testBytes), i)

			StringDecodeQuotedPrintableString(test)

		}
	}
}

func BenchmarkDecodeQuotedPrintableByte(b *testing.B) {

	testBytes := []byte("Dette er en test fra =C3=85rhus")

	// number of tests
	for i := 0; i < b.N; i++ {

		// run functions
		for i := range []int{1, 20, 30, 50, 100, 2000} {
			test := []byte(StringRepeat(string(testBytes), i))

			StringDecodeQuotedPrintableBytes(test)

		}
	}
}

func BenchmarkStringRemove(b *testing.B) {
	for i := 0; i < b.N; i++ {
		StringRemove([]string{"one", "two", "three", "four"}, "two")
	}
}
